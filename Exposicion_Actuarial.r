###################################################################################
### Si accedemos a la base por primera vez - DESCARGAMOS LA BASE###################
###################################################################################

download_data = function(){
temp = tempdir()
tf = tempfile(tmpdir = temp, fileext = ".zip")
download.file("https://www.sbs.gob.pe/tmortalidad/bases_de_datos_tm.zip", tf)

fname_invalido = unzip(tf,list = T)$Name[2]
fname_sano = unzip(tf,list = T)$Name[3]
unzip(tf, files=fname_sano, exdir=temp, overwrite = T)
unzip(tf, files=fname_invalido, exdir=temp, overwrite = T)
fpath1 = file.path(temp,fname_sano)
fpath2 = file.path(temp,fname_invalido)
data = rbind(read.csv(fpath1, sep = "\t"),read.csv(fpath2,sep = "\t"))

###################################################################################
### cambia el formato de texto a fecha#############################################
###################################################################################
data$tm_fec_nac=as.Date(data$tm_fec_nac, format = "%d/%m/%Y")
data$tm_fec_vig=as.Date(data$tm_fec_vig, format = "%d/%m/%Y")
data$tm_fec_fall=as.Date(data$tm_fec_fall, format = "%d/%m/%Y")
data$finexposic=as.Date(data$finexposic, format = "%d/%m/%Y")

data_sanos = data[data$tm_salud == 0, ]
data_invalidos = data[data$tm_salud != 0, ]
saveRDS(data,"D:/total.rds")
saveRDS(data_sanos,"D:/data_sanos.rds")
saveRDS(data_invalidos,"D:/data_invalidos.rds")
rm(list=c(ls()[ls()!="data"]))
gc()
return(data)
}

###################################################################################
### nombramos a la base de datos con nombre DATA ##################################
###################################################################################
data = download_data()
###################################################################################
###################################################################################
##veamos los 1000 primeros datos de la tabla ya que la base es muy grande ######### 
## y se demora demasiado al cargar los datos ######################################
###################################################################################
View(data[0:1000,])
###################################################################################


###################################################################################
# Si ya contamos con la base de datos descargada###################################
###################################################################################
#data = readRDS("D:/total.rds")
#data_sanos = readRDS("D:/data_sanos.rds")
#data_invalidos = readRDS("D:/data_invalidos.rds")

###################################################################################
##para poner un grafico en una ventana#############################################
###################################################################################
par(mfrow = c(3,1))
par(mfrow = c(1,1))
###################################################################################
### grafico de distribucion porcentual de la relacion familiar#####################
###################################################################################

###################################################################################
### grafico de distribucion porcentual de la relacion familiar sanos###############
###################################################################################
	 barplot(table(data_sanos$tm_cod_rel)/dim(data_sanos)[1],
			ylim = c(0,1),
 			border = F, 
			main = "Distribucion por relacion familiar de las personas sanas del SPP,
					tanto de pensionistas como de afiliados activos que contribuyen o
		 			contribuyeron con aportes al SPP",
			col=c("coral","coral1","coral2","coral3","coral4"),
			legend.text=c("conyuge","hijos","padres","titulares","titulares en el
												 padron del SPP"),
			xlim=c(0,10),
			xlab="RELACION FAMILIAR",
			ylab="FRECUENCIA EN %",
			cex.lab = 0.7, 
			cex.main = 0.8
				)

###################################################################################
### grafico de distribucion porcentual de la relacion familiar invalidos ##########
###################################################################################
	barplot(table(data_invalidos$tm_cod_rel)/dim(data_invalidos)[1],
		 ylim = c(0,1),
 		border = F, 
		main = "Distribucion por relacion familiar de las personas invalidas del SPP,
			tanto de pensionistas como de afiliados activos que contribuyen o
		 	contribuyeron con aportes al SPP",
		col=c("coral","coral1","coral2","coral3","coral4"),
		legend.text=c("conyuge","hijos","padres","titulares","titulares en el
											 padron del SPP"),
		xlim=c(0,10),
		xlab="RELACION FAMILIAR",
		ylab="FRECUENCIA EN %",
		cex.lab = 0.7, 
		cex.main = 0.8

		)
###################################################################################
### grafico de distribucion porcentual de la relacion familiar total###############
###################################################################################
	barplot(table(data$tm_cod_rel)/dim(data)[1],
		 ylim = c(0,1),
 		border = F, 
		main = "Distribucion por relacion familiar de las personas sanas e invalidas 
			del SPP,tanto de pensionistas como de afiliados activos que contribuyen o
		 	contribuyeron con aportes al SPP",
		col=c("coral","coral1","coral2","coral3","coral4"),
		legend.text=c("conyuge","hijos","padres","titulares","titulares en el
											 padron del SPP"),
		xlim=c(0,10),
		xlab="RELACION FAMILIAR",
		ylab="FRECUENCIA EN %",
		cex.lab = 0.7, 
		cex.main = 0.8

		)

###################################################################################
### grafico de distribucion porcentual por sexo####################################
###################################################################################

###################################################################################
### grafico de distribucion porcentual por sexo sanos #############################
###################################################################################

	barplot(table(data_sanos$tm_sexo)/dim(data_sanos)[1],
		 border = F,
		 main = 	"Distribucion por sexo de las personas sanas e invalidas del SPP,
				tanto de pensionistas como de afiliados activos que contribuyen o
		 		contribuyeron con aportes al SPP",
		col=c("pink","blue"),
		legend.text=c("mujeres","varones"),
		xlim=c(0,4),
		xlab="SEXO",
		ylab="FRECUENCIA EN %",
		cex.lab = 0.7, 
		cex.main = 0.8
		)

###################################################################################
### grafico de distribucion porcentual por sexo invalidos #########################
###################################################################################

	barplot(table(data_invalidos$tm_sexo)/dim(data_invalidos)[1],
		 border = F,
		 main = 	"Distribucion por sexo de las personas sanas e invalidas del SPP,
				tanto de pensionistas como de afiliados activos que contribuyen o
		 		contribuyeron con aportes al SPP",
		col=c("pink","blue"),
		legend.text=c("mujeres","varones"),
		xlim=c(0,4),
		xlab="SEXO",
		ylab="FRECUENCIA EN %",
		cex.lab = 0.7, 
		cex.main = 0.8
		)

###################################################################################
### grafico de distribucion porcentual por sexo total #############################
###################################################################################

	barplot(table(data$tm_sexo)/dim(data)[1],
		 border = F,
		 main = 	"Distribucion por sexo de las personas sanas e invalidas del SPP,
				tanto de pensionistas como de afiliados activos que contribuyen o
		 		contribuyeron con aportes al SPP",
		col=c("pink","blue"),
		legend.text=c("mujeres","varones"),
		xlim=c(0,4),
		xlab="SEXO",
		ylab="FRECUENCIA EN %",
		cex.lab = 0.7, 
		cex.main = 0.8
		)


###################################################################################
###############################NACIMIENTOS#########################################
###################################################################################

###################################################################################
## pongamos en una tabla la cantidad de personas nacidas en cada año###############
###################################################################################
nacidastotal <- table(as.numeric(format(data$tm_fec_nac,"%Y")))
nacidassanos <- table(as.numeric(format(data_sanos$tm_fec_nac,"%Y")))
nacidasinvalidos <- table(as.numeric(format(data_invalidos$tm_fec_nac,"%Y")))
###################################################################################
## ahora visualizamos la tabla ####################################################
###################################################################################
View(nacidastotal )
View(nacidassanos )
View(nacidasinvalidos )

###################################################################################

###################################################################################
##para poner dos grafico en una ventana############################################
###################################################################################
par(mfrow = c(2,1))
###################################################################################
dim(data_invalidos)
###################################################################################
### grafico del histograma del año de nacimiento de las personas###################
###################################################################################

###################################################################################
### Histograma para el año de nacimiento de las personas sanas e invalidas ########
###################################################################################
	par(mfrow = c(2,1))
	hist(as.numeric(format(data$tm_fec_nac,"%Y")),
		nclass = 20, 
		col = "firebrick4", 
		border = F, 
		xlab = "Año de nacimiento", 
		ylab = "Frecuencia", 
		main="Histograma para el año de nacimiento de las 
			personas sanas e invalidas del SPP,tanto de pensionistas como de 
			afiliados activos que contribuyen o contribuyeron con aportes al SPP", 
		cex.lab = 0.7, 
		cex.main = 0.8
		)

	boxplot(as.numeric(format(data$tm_fec_nac,"%Y")), 
		horizontal = T, 
		col = "firebrick3",
		cex.main=0.8, 
		main="Diagrama de cajas del año de nacimiento de las 
			personas sanas e invalidas del SPP,tanto de pensionistas como de 
			afiliados activos que contribuyen o contribuyeron con aportes al SPP", 
		cex.lab = 0.7
		)
###################################################################################
### Histograma para el año de nacimiento de las personas sanas ####################
###################################################################################
	par(mfrow = c(2,1))
	hist(as.numeric(format(data_sanos$tm_fec_nac,"%Y")),
		nclass = 20, 
		col = "firebrick4", 
		border = F, 
		xlab = "Año de nacimiento", 
		ylab = "Frecuencia", 
		main="Histograma para el año de nacimiento de las 
			personas sanas del SPP,tanto de pensionistas como de 
			afiliados activos que contribuyen o contribuyeron con aportes al SPP", 
		cex.lab = 0.7, 
		cex.main = 0.8
		)
	
	boxplot(as.numeric(format(data_sanos$tm_fec_nac,"%Y")), 
		horizontal = T, 
		col = "firebrick3",
		cex.main=0.8, 
		main="Diagrama de cajas del año de nacimiento de las 
			personas sanas del SPP,tanto de pensionistas como de 
			afiliados activos que contribuyen o contribuyeron con aportes al SPP", 
		cex.lab = 0.7
		)

###################################################################################
### Histograma para el año de nacimiento de las personas invalidas ################
###################################################################################
	par(mfrow = c(2,1))
	hist(as.numeric(format(data_invalidos$tm_fec_nac,"%Y")),
		nclass = 20, 
		col = "firebrick4", 
		border = F, 
		xlab = "Año de nacimiento", 
		ylab = "Frecuencia", 
		main="Histograma para el año de nacimiento de las 
			personas invalidas del SPP,tanto de pensionistas como de 
			afiliados activos que contribuyen o contribuyeron con aportes al SPP", 
		cex.lab = 0.7, 
		cex.main = 0.8
		)
	
	boxplot(as.numeric(format(data_invalidos$tm_fec_nac,"%Y")), 
		horizontal = T, 
		col = "firebrick3",
		cex.main=0.8, 
		main="Diagrama de cajas del año de nacimiento de las 
			personas invalidas del SPP,tanto de pensionistas como de 
			afiliados activos que contribuyen o contribuyeron con aportes al SPP", 
		cex.lab = 0.7
		)


###################################################################################
### Distribucion para el año de nacimiento  #######################################
###################################################################################
	par(mfrow = c(1,1)); 
		plot(table(as.numeric(format(data$tm_fec_nac,"%Y"))),
		type='l', 
		col = "firebrick4",
		xlab = "Año de nacimiento", 
		ylab = "Frecuencia", 
		main = "Distribucion para el año de nacimiento", 
		cex.lab = 0.7,
		cex.main=0.8
		)
	
###################################################################################
###################################################################################
###############################FALLECIMIENTOS######################################
###################################################################################

###################################################################################
## pongamos en una tabla la cantidad de personas nacidas en cada año###############
###################################################################################
fallecidastotal <- table(as.numeric(format(data$tm_fec_fall,"%Y")))
fallecidassanos <- table(as.numeric(format(data_sanos$tm_fec_fall,"%Y")))
fallecidasinvalidos <- table(as.numeric(format(data_invalidos$tm_fec_fall,"%Y")))
###################################################################################
## ahora visualizamos la tabla ####################################################
###################################################################################
View(fallecidastotal)
View(fallecidassanos)
View(fallecidasinvalidos)
###################################################################################
###################################################################################
### grafico del histograma del año de fallecimiento de las personas################
###################################################################################

###################################################################################
### Histograma para el año de fallecimiento de las personas sanas e invalidas #####
###################################################################################

par(mfrow = c(2,1))
	hist(as.numeric(format(data$tm_fec_fall,"%Y")),
		nclass = 20, 
		col = "lightblue4", 
		border = F, 
		xlab = "Año de fallecimiento", 
		ylab = "Frecuencia", 
		main="histograma del año de fallecimeinto de las 
			personas sanas e invalidas del SPP,tanto de pensionistas como de 
			afiliados activos que contribuyen o contribuyeron con aportes al SPP", 
		cex.lab = 0.7, 
		cex.main = 0.8
		)

	boxplot(as.numeric(format(data$tm_fec_fall,"%Y")), 
		horizontal = T, 
		col = "skyblue",
		cex.main=0.8, 
		main="Diagrama de Cajas del año de fallecimientode las 
			personas sanas e invalidas del SPP,tanto de pensionistas como de 
			afiliados activos que contribuyen o contribuyeron con aportes al SPP",
		cex.lab = 0.7
		)
###################################################################################
### Histograma para el año de fallecimiento de las personas sanas #################
###################################################################################

par(mfrow = c(3,1))
	hist(as.numeric(format(data_sanos$tm_fec_fall,"%Y")),
		nclass = 10, 
		col = "lightblue4", 
		border = F, 
		xlab = "Año de fallecimiento", 
		ylab = "Frecuencia", 
		main="histograma del año de fallecimeinto de las 
			personas sanas del SPP,tanto de pensionistas como de 
			afiliados activos que contribuyen o contribuyeron con aportes al SPP", 
		cex.lab = 0.7, 
		cex.main = 0.8
		)

	boxplot(as.numeric(format(data_sanos$tm_fec_fall,"%Y")), 
		horizontal = T, 
		col = "skyblue",
		cex.main=0.8, 
		main="Diagrama de Cajas del año de fallecimientode las 
			personas sanas del SPP,tanto de pensionistas como de 
			afiliados activos que contribuyen o contribuyeron con aportes al SPP",
		cex.lab = 0.7
		)

###################################################################################
### Histograma para el año de fallecimiento de las personas invalidas  ############
###################################################################################

par(mfrow = c(2,1))
	hist(as.numeric(format(data_invalidos$tm_fec_fall,"%Y")),
		nclass = 20, 
		col = "lightblue4", 
		border = F, 
		xlab = "Año de fallecimiento", 
		ylab = "Frecuencia", 
		main="histograma del año de fallecimeinto de las 
			personas invalidas del SPP,tanto de pensionistas como de 
			afiliados activos que contribuyen o contribuyeron con aportes al SPP", 
		cex.lab = 0.7, 
		cex.main = 0.8
		)

	boxplot(as.numeric(format(data_invalidos$tm_fec_fall,"%Y")), 
		horizontal = T, 
		col = "skyblue",
		cex.main=0.8, 
		main="Diagrama de Cajas del año de fallecimientode las 
			personas invalidas del SPP,tanto de pensionistas como de 
			afiliados activos que contribuyen o contribuyeron con aportes al SPP",
		cex.lab = 0.7
		)
###################################################################################
### Distribucion para el año de fallecimeintos ####################################
###################################################################################

par(mfrow = c(1,1)); 
	plot(table(as.numeric(format(data$tm_fec_fall,"%Y"))),
		type='l', 
		xlab = "Año de fallecimiento", 
		ylab = "Frecuencia", 
		main = "Distribucion para el año de fallecimiento", 
		cex.lab = 0.7,
		cex.main=0.8
		)

###################################################################################
###################################################################################
###############################mas graficas #######################################
###################################################################################
###################################################################################

#######################################################################################
#############edad de muerte de las personas pertenecientes al SPP
#######################################################################################
	diff_edad=function(inicio,fin){
	ano=as.numeric(format(inicio,"%Y"))-as.numeric(format(fin,"%Y"))
	mes=as.numeric(format(inicio,"%m"))-as.numeric(format(fin,"%m"))
	dia=as.numeric(format(inicio,"%d"))-as.numeric(format(fin,"%d"))
	X=ano+mes/12+dia/365.25
	return(X)
	}
	data$edad_muerte=diff_edad(data$tm_fec_fall,data$tm_fec_nac)
	
View(data[0:1000,])
par(mfrow = c(2,1))
	hist(data$edad_muerte,
		nclass = 10, 
		col = "olivedrab4", 
		border = F, 
		xlab = "Edad de fallecimientos", 
		ylab = "Frecuencia", 
		main="histograma de las edades de fallecimeinto de las 
			personas sanas e invalidas del SPP,tanto de pensionistas como de 
			afiliados activos que contribuyen o contribuyeron con aportes al SPP", 
		cex.lab = 0.7, 
		cex.main = 0.8
		)

	boxplot(data$edad_muerte, 
		horizontal = T, 
		col = "olivedrab4",
		cex.main=0.8, 
		main="Diagrama de Cajas de las edades de fallecimeinto de las 
			personas sanas e invalidas del SPP,tanto de pensionistas como de 
			afiliados activos que contribuyen o contribuyeron con aportes al SPP"
		)

View(data[0:1000,])
summary(data$edad_muerte)
		 Min. 1st Qu.  Median 
    		  0      38      51 
 		  Mean 3rd Qu.    Max. 
  		   51      63     106 
  

#######################################################################################
#############edad de muerte de las personas sanas pertenecientes al SPP
#######################################################################################
data_sanos$edad_muerte=diff_edad(data_sanos$tm_fec_fall,data_sanos$tm_fec_nac)
par(mfrow = c(2,1))
	hist(data_sanos$edad_muerte,
		nclass = 10, 
		col = "olivedrab4", 
		border = F, 
		xlab = "Edad de fallecimientos", 
		ylab = "Frecuencia", 
		main="histograma de las edades de fallecimeinto de las 
			personas sanas del SPP,tanto de pensionistas como de 
			afiliados activos que contribuyen o contribuyeron con aportes al SPP", 
		cex.lab = 0.7, 
		cex.main = 0.8
		)

	boxplot(data_sanos$edad_muerte, 
		horizontal = T, 
		col = "olivedrab4",
		cex.main=0.8, 
		main="Diagrama de Cajas de las edades de fallecimeinto de las 
			personas sanas del SPP,tanto de pensionistas como de 
			afiliados activos que contribuyen o contribuyeron con aportes al SPP"
		)

View(data_sanos[0:1000,])
summary(data_sanos$edad_muerte)
 		Min. 1st Qu.  Median 
   		   0      38      50 
 		  Mean 3rd Qu.    Max. 
   		  51      63     106 
 		

#######################################################################################
#############edad de muerte de las personas invalidas pertenecientes al SPP
#######################################################################################
data_invalidos$edad_muerte=diff_edad(data_invalidos$tm_fec_fall,data_invalidos$tm_fec_nac)
par(mfrow = c(2,1))
	hist(data_invalidos$edad_muerte,
		nclass = 10, 
		col = "olivedrab4", 
		border = F, 
		xlab = "Edad de fallecimientos", 
		ylab = "Frecuencia", 
		main="histograma de las edades de fallecimeinto de las 
			personas invalidos del SPP,tanto de pensionistas como de 
			afiliados activos que contribuyen o contribuyeron con aportes al SPP", 
		cex.lab = 0.7, 
		cex.main = 0.8
		)

	boxplot(data_invalidos$edad_muerte, 
		horizontal = T, 
		col = "olivedrab4",
		cex.main=0.8, 
		main="Diagrama de Cajas de las edades de fallecimeinto de las 
			personas invalidos del SPP,tanto de pensionistas como de 
			afiliados activos que contribuyen o contribuyeron con aportes al SPP"
		)

View(data_invalidos[0:1000,])
summary(data_invalidos$edad_muerte)
		 Min. 1st Qu.  Median 
  		 0.00   48.20   56.34 
  		 Mean 3rd Qu.    Max. 
 		 54.66   62.54   92.40
###################################################################################
###################################################################################
###################################################################################
#############edad de fin de exposicion personas sanas e invalidas##################
###################################################################################
###################################################################################
	
data$edad_finexpo=diff_edad(data$finexposic,data$tm_fec_nac)
par(mfrow = c(2,1))
	hist(data$edad_finexpo,
		nclass = 10, 
		col = "yellowgreen", 
		border = F, 
		xlab = "Edad de fin de exposicion", 
		ylab = "Frecuencia", 
		main="histograma de las edades de fin de exposicion de las 
			personas sanas e invalidas del SPP,tanto de pensionistas como de 
			afiliados activos que contribuyen o contribuyeron con aportes al SPP", 
		cex.lab = 0.7, 
		cex.main = 0.8
		)

	boxplot(data$edad_finexpo, 
		horizontal = T, 
		col = "yellowgreen",
		cex.main=0.8, 
		main="Diagrama de Cajas de las edades de fin de exposicion de las 
			personas sanas e invalidas del SPP,tanto de pensionistas como de 
			afiliados activos que contribuyen o contribuyeron con aportes al SPP"
		)

View(data[0:1000,])
summary(data$edad_finexpo)
		  Min. 1st Qu.  Median 
   		  19      52      56 
  		 Mean 3rd Qu.    Max. 
    		 56      61      96 

  

#######################################################################################
#############edad de fin de exposicion de las personas sanas pertenecientes al SPP
#######################################################################################
data_sanos$edad_finexpo=diff_edad(data_sanos$finexposic,data_sanos$tm_fec_nac)
par(mfrow = c(2,1))
	hist(data_sanos$edad_finexpo,
		nclass = 10, 
		col = "yellowgreen", 
		border = F, 
		xlab = "Edad de fin de exposicion", 
		ylab = "Frecuencia", 
		main="histograma de las edades de fin de exposicion de las 
			personas sanas del SPP,tanto de pensionistas como de 
			afiliados activos que contribuyen o contribuyeron con aportes al SPP", 
		cex.lab = 0.7, 
		cex.main = 0.8
		)

	boxplot(data_sanos$edad_finexpo, 
		horizontal = T, 
		col = "yellowgreen",
		cex.main=0.8, 
		main="Diagrama de Cajas de las edades de fin de exposicion de las 
			personas sanas del SPP,tanto de pensionistas como de 
			afiliados activos que contribuyen o contribuyeron con aportes al SPP"
		)

View(data_sanos[0:1000,])
summary(data_sanos$edad_finexpo)
		  Min. 1st Qu.  Median 
   		  19      52      56 
  		 Mean 3rd Qu.    Max. 
   		  56      61      96 

 		

#######################################################################################
#############edad de fin de exposicion de las personas invalidas pertenecientes al SPP
#######################################################################################
data_invalidos$edad_finexpo=diff_edad(data_invalidos$finexposic,data_invalidos$tm_fec_nac)
par(mfrow = c(2,1))
	hist(data_invalidos$edad_finexpo,
		nclass = 10, 
		col = "yellowgreen", 
		border = F, 
		xlab = "Edad de fin de exposicion", 
		ylab = "Frecuencia", 
		main="histograma de las edades de fin de exposicion de las 
			personas invalidas del SPP,tanto de pensionistas como de 
			afiliados activos que contribuyen o contribuyeron con aportes al SPP", 
		cex.lab = 0.7, 
		cex.main = 0.8
		)

	boxplot(data_invalidos$edad_finexpo, 
		horizontal = T, 
		col = "yellowgreen",
		cex.main=0.8, 
		main="Diagrama de Cajas de las edades de fin de exposicion de las 
			personas invalidas del SPP,tanto de pensionistas como de 
			afiliados activos que contribuyen o contribuyeron con aportes al SPP"
		)

View(data_invalidos[0:1000,])
summary(data_invalidos$edad_finexpo)
		   Min. 1st Qu.  Median 
 		 22.58   41.33   48.71 
 		  Mean 3rd Qu.    Max. 
  		48.40   56.27   75.74 

###################################################################################
###################################################################################
###################################################################################
#############tiempo de aportaciones al SPP ########################################
###################################################################################
###################################################################################

###################################################################################
#############tiempo de aportaciones al SPP hasta su fallesimiento##################
###################################################################################
	
data$tiempo_aporfalle=diff_edad(data$tm_fec_fall,data$tm_fec_vig)
par(mfrow = c(2,1))
	hist(data$tiempo_aporfalle,
		nclass = 20, 
		col = "orangered4", 
		border = F, 
		xlab = "tiempo de aportaciones", 
		ylab = "Frecuencia", 
		main="histograma del tiempo de aportaciones de las 
			personas sanas e invalidas hasta su fallecimeinto del SPP,tanto de
			 pensionistas como de afiliados activos que contribuyen o contribuyeron 
			con aportes al SPP", 
		cex.lab = 0.7, 
		cex.main = 0.8
		)

	boxplot(data$tiempo_aporfalle, 
		horizontal = T, 
		col = "orangered4",
		cex.main=0.8, 
		main="Diagrama de Cajas del tiempo de aportaciones de las 
			personas sanas e invalidas hasta su fallecimeinto del SPP,tanto de
			 pensionistas como de afiliados activos que contribuyen o contribuyeron 
			con aportes al SPP"
		)

View(data[0:1000,])
summary(data$tiempo_aporfalle)
		   Min. 1st Qu.  Median 
   		 -66       6      11 
  		 Mean 3rd Qu.    Max. 
   		  11      17      24 
#######################################################################################
View(data[data$tiempo_aporfalle<0 & !is.na(data$tiempo_aporfalle),])

  

#######################################################################################
#############tiempo de aportaciones de sanos al SPP hasta su fallesimiento#############
#######################################################################################
data_sanos$tiempo_aporfalle=diff_edad(data_sanos$tm_fec_fall,data_sanos$tm_fec_vig)
par(mfrow = c(2,1))
	hist(data_sanos$tiempo_aporfalle,
		nclass = 10, 
		col = "yellowgreen", 
		border = F, 
		xlab = "tiempo de aportaciones", 
		ylab = "Frecuencia", 
		main="histograma del tiempo de aportaciones de las 
			personas sanas hasta su fallecimeinto del SPP,tanto de
			 pensionistas como de afiliados activos que contribuyen o contribuyeron 
			con aportes al SPP", 
		cex.lab = 0.7, 
		cex.main = 0.8
		)

	boxplot(data_sanos$tiempo_aporfalle, 
		horizontal = T, 
		col = "yellowgreen",
		cex.main=0.8, 
		main="Diagrama de Cajas del tiempo de aportaciones de las 
			personas sanas hasta su fallecimeinto del SPP,tanto de
			 pensionistas como de afiliados activos que contribuyen o contribuyeron 
			con aportes al SPP"
		)

View(data_sanos[1000:10000,])
summary(data_sanos$tiempo_aporfalle)
		  Min. 1st Qu.  Median 
  		  -66       6      11 
  		 Mean 3rd Qu.    Max. 
    		 11      17      24 

 		

#######################################################################################
#############tiempo de aportaciones de sanos al SPP hasta su fallesimiento#############
#######################################################################################
data_invalidos$tiempo_aporfalle=diff_edad(data_invalidos$tm_fec_fall,data_invalidos$tm_fec_vig)
par(mfrow = c(2,1))
	hist(data_invalidos$tiempo_aporfalle,
		nclass = 10, 
		col = "yellowgreen", 
		border = F, 
		xlab = "tiempo de aportaciones", 
		ylab = "Frecuencia", 
		main="histograma del tiempo de aportaciones de las 
			personas invalidas hasta su fallecimeinto del SPP,tanto de
			 pensionistas como de afiliados activos que contribuyen o contribuyeron 
			con aportes al SPP", 
		cex.lab = 0.7, 
		cex.main = 0.8
		)

	boxplot(data_invalidos$tiempo_aporfalle, 
		horizontal = T, 
		col = "yellowgreen",
		cex.main=0.8, 
		main="Diagrama de Cajas del tiempo de aportaciones de las 
			personas invalidas hasta su fallecimeinto del SPP,tanto de
			 pensionistas como de afiliados activos que contribuyen o contribuyeron 
			con aportes al SPP"
		)

View(data_invalidos[0:1000,])
summary(data_invalidos$tiempo_aporfalle)
		   Min. 1st Qu.  Median 
 		-46.91   11.21   15.90 
  		 Mean 3rd Qu.    Max. 
  		14.98   19.54   23.54 


###################################################################################
#############tiempo de aportaciones al SPP hasta su fin de exposicion##############
###################################################################################
	
data$tiempo_aporfin=diff_edad(data$finexposic,data$tm_fec_vig)
par(mfrow = c(2,1))
	hist(data$tiempo_aporfin,
		nclass = 20, 
		col = "orangered4", 
		border = F, 
		xlab = "tiempo de aportaciones", 
		ylab = "Frecuencia", 
		main="histograma del tiempo de aportaciones de las 
			personas sanas e invalidas hasta su fin de exposicion del SPP,tanto de
			 pensionistas como de afiliados activos que contribuyen o contribuyeron 
			con aportes al SPP", 
		cex.lab = 0.7, 
		cex.main = 0.8
		)

	boxplot(data$tiempo_aporfin, 
		horizontal = T, 
		col = "orangered4",
		cex.main=0.8, 
		main="Diagrama de Cajas del tiempo de aportaciones de las 
			personas sanas e invalidas hasta su fin de exposicion del SPP,tanto de
			 pensionistas como de afiliados activos que contribuyen o contribuyeron 
			con aportes al SPP"
		)

View(data[0:1000,])
summary(data$tiempo_aporfin)
		   Min. 1st Qu.  Median 
   		  -4      14      17 
  		 Mean 3rd Qu.    Max. 
   		  16      19      24 

data$tiempo_aporfin=diff_edad(data$finexposic,data$tm_fec_vig)
View(data[data$tiempo_aporfin<0 & !is.na(data$tiempo_aporfin),])
  

#######################################################################################
#############tiempo de aportaciones al SPP hasta su fin de exposicion##############
#######################################################################################
data_sanos$tiempo_aporfin=diff_edad(data_sanos$finexposic,data_sanos$tm_fec_vig)
par(mfrow = c(2,1))
	hist(data_sanos$tiempo_aporfin,
		nclass = 20, 
		col = "orangered4", 
		border = F, 
		xlab = "tiempo de aportaciones", 
		ylab = "Frecuencia", 
		main="histograma del tiempo de aportaciones de las 
			personas sanas hasta su fin de exposicion del SPP,tanto de
			 pensionistas como de afiliados activos que contribuyen o contribuyeron 
			con aportes al SPP", 
		cex.lab = 0.7, 
		cex.main = 0.8
		)

	boxplot(data_sanos$tiempo_aporfin, 
		horizontal = T, 
		col = "orangered4",
		cex.main=0.8, 
		main="Diagrama de Cajas del tiempo de aportaciones de las 
			personas sanas  hasta su fin de exposicion del SPP,tanto de
			 pensionistas como de afiliados activos que contribuyen o contribuyeron 
			con aportes al SPP"
		)

View(data_sanos[0:1000,])
summary(data_sanos$tiempo_aporfin)
		   Min. 1st Qu.  Median 
   		  -4      14      17 
  		 Mean 3rd Qu.    Max. 
   		  16      19      24 


 		

#######################################################################################
#############tiempo de aportaciones al SPP hasta su fin de exposicion##############
#######################################################################################
data_invalidos$tiempo_aporfin=diff_edad(data_invalidos$finexposic,data_invalidos$tm_fec_vig)
par(mfrow = c(2,1))
	hist(data_invalidos$tiempo_aporfin,
		nclass = 20, 
		col = "orangered4", 
		border = F, 
		xlab = "tiempo de aportaciones", 
		ylab = "Frecuencia", 
		main="histograma del tiempo de aportaciones de las 
			personas invalidas hasta su fin de exposicion del SPP,tanto de
			 pensionistas como de afiliados activos que contribuyen o contribuyeron 
			con aportes al SPP", 
		cex.lab = 0.7, 
		cex.main = 0.8
		)

	boxplot(data_invalidos$tiempo_aporfin, 
		horizontal = T, 
		col = "orangered4",
		cex.main=0.8, 
		main="Diagrama de Cajas del tiempo de aportaciones de las 
			personas invalidas hasta su fin de exposicion del SPP,tanto de
			 pensionistas como de afiliados activos que contribuyen o contribuyeron 
			con aportes al SPP"
		)

View(data_invalidos[0:1000,])
summary(data_invalidos$tiempo_aporfin)
		    Min. 1st Qu.  Median 
 		 2.038  10.678  14.456 
 		  Mean 3rd Qu.    Max. 
 		14.132  18.313  23.566 



##############################################################por revisar##########
###################################################################################
###############################del papper   #######################################
###################################################################################
###################################################################################

## edad al momento del inicio de la exposicion
##################################################################################
##para las 3 tablas (sanos, invalidos y la combinada)
##################################################################################

diff_edad=function(inicio,fin){
ano=as.numeric(format(inicio,"%Y"))-as.numeric(format(fin,"%Y"))
mes=as.numeric(format(inicio,"%m"))-as.numeric(format(fin,"%m"))
dia=as.numeric(format(inicio,"%d"))-as.numeric(format(fin,"%d"))
X=ano+mes/12+dia/365.25
return(X)
}

data_sanos$edad_exp=diff_edad(data_sanos$tm_fec_vig,data_sanos$tm_fec_nac)
data_invalidos$edad_exp=diff_edad(data_invalidos$tm_fec_vig,data_invalidos$tm_fec_nac)
data$edad_exp=diff_edad(data$tm_fec_vig,data$tm_fec_nac)

View(data_sanos[0:1000,])
View(data_invalidos[0:1000,])
View(data[0:1000,])

##################################################################################
##graficando el histograma y diagrama de cajas de la edad al incio de la exposicion
##################################################################################

###### Grafico para la edad al inicio de la exposicion para las personas sanas
##################################################################################
par(mfrow = c(2,1))
	hist(data_sanos$edad_exp,
		nclass = 20, 
		col = "darkcyan", 
		border = F, 
		xlab = "Edad exacta al momento de iniciar la exposicion", 
		ylab = "Frecuencia", 
		main="Histograma de la Edad al momento de inicio de vigencia", 
		cex.lab = 0.7, 
		cex.main = 0.8
		)


	boxplot(data_sanos$edad_exp, 
		horizontal = T, 
		col = "darkcyan",
		cex.main=0.8, 
		main="Diagrama de Cajas de la Edad al momento de inicio de vigencia"
		)
#par(mfrow = c(1,1)); plot(table(trunc(data_sanos$edad_exp)),type='l')
######

###### Grafico para la edad al inicio de la exposicion para las personas invalidas
###################################################################################
par(mfrow = c(2,1))
	hist(data_invalidos$edad_exp,
		nclass = 20, 
		col = "darkcyan", 
		border = F, 
		xlab = "Edad exacta al momento de iniciar la exposicion", 
		ylab = "Frecuencia", 
		main="Histograma de la Edad al momento de inicio de vigencia", 
		cex.lab = 0.7, 
		cex.main = 0.8
		)


	boxplot(data_invalidos$edad_exp, 
		horizontal = T, 
		col = "darkcyan",
		cex.main=0.8, 
		main="Diagrama de Cajas de la Edad al momento de inicio de vigencia"
		)
#par(mfrow = c(1,1)); plot(table(trunc(data_invalidos$edad_exp)),type='l')
######


###### Grafico para la edad al inicio de la exposicion para sanos e invalidos
###################################################################################
par(mfrow = c(2,1))
	hist(data$edad_exp,
		nclass = 20, 
		col = "darkcyan", 
		border = F, 
		xlab = "Edad exacta al momento de iniciar la exposicion", 
		ylab = "Frecuencia", 
		main="Histograma de la Edad al momento de inicio de vigencia", 
		cex.lab = 0.7, 
		cex.main = 0.8
		)


	boxplot(data$edad_exp, 
		horizontal = T, 
		col = "darkcyan",
		cex.main=0.8, 
		main="Diagrama de Cajas de la Edad al momento de inicio de vigencia"
		)
#par(mfrow = c(1,1)); plot(table(trunc(data$edad_exp)),type='l')
######

###################################################################################
###################################################################################
###############################COMPARACIONES#######################################
###################################################################################
###################################################################################
###################################################################################
###################################################################################
###################################################################################
###################################################################################
###################################################################################


###################################################################################
###############################COMPARACIONES#######################################
###################################################################################


#### COMPARAMOS 
###################################################################################



# Bucle para generar las variables F1, F2, F3 y F4 para la exposicion
for (year in 2002:2017){
	ini = as.Date(paste("01/01/",year,sep=""), format = "%d/%m/%Y")
	fin = as.Date(paste("31/12/",year,sep=""), format = "%d/%m/%Y")
	cumple = as.Date(paste(format(data$tm_fec_nac,"%d/%m"),year,sep="/"), format="%d/%m/%Y")
	cumple_1 = as.Date(paste(format(data$tm_fec_nac,"%d/%m"),year+1,sep="/"), format="%d/%m/%Y")

	data["f1"] = pmin(
				pmax(
				ini, data$tm_fec_nac, data$tm_fec_vig
				),
			 fin
			 )
					
	data["f2"] = b = as.Date(ifelse(!is.na(data$tm_fec_fall) & data$tm_fec_fall < cumple_1 &
						data$tm_fec_fall < fin & data$tm_fec_fall > ini,
						cumple,
						ifelse(!is.na(data$finexposic) & data$finexposic < cumple_1 &
						data$finexposic < fin & data$finexposic > ini, data$finexposic, 
						cumple
						)
						)
						, origin="1970-01-01")

	data["f3"] = pmax(b, ini, data$tm_fec_vig)
			 		
	data["f4"] = as.Date(pmax(
			pmin(
				ifelse(is.na(data$finexposic),fin,data$finexposic), fin
				),
		   ini
		   ), origin="1970-01-01")

	q=paste("exp_",year,sep="")
	exp1 =(data["f2"] - data["f1"]) / 365.25
	exp1[exp1<0]=0
	names(exp1) = q
	exp2 =(data["f4"] - data["f3"]) / 365.25
	exp2[exp2<0]=0
	names(exp2) = q
		
	s = paste("edad_",year,sep="")
	edad1 = as.data.frame(trunc((ini - data$tm_fec_nac)/365.25))
	names(edad1)=s
	edad2 = as.data.frame(trunc((fin - data$tm_fec_nac)/365.25))
	names(edad2)=s

tab1 = cbind(edad1, exp1)
tab2 = cbind(edad2, exp2)
tab1[q][is.na(tab1[q])]=0
tab2[q][is.na(tab2[q])]=0
ans1=tapply(as.numeric(tab1[,2]),as.numeric(tab1[,1]),sum)
ans2=tapply(as.numeric(tab2[,2]),as.numeric(tab2[,1]),sum)

ans = merge(x = ans1, y = ans2, by = 0, all = TRUE)
ans[,2][is.na(ans[,2])]=0
ans[,3][is.na(ans[,3])]=0
ans$exp = ans$x + ans$y

write.csv(ans, paste('D:/exp_', year, '.csv', sep=''))
print(paste("Ya se proceso el año",year))
        }