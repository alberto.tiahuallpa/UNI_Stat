options(scipen=999)

getwd()
rm(list=ls())
setwd("C:/Users/PC01/Downloads/Tasas Brutas/Tasas Brutas")
#################################################################
##Analisis Tasas Brutas de Mortalidad
tinv_alb<-read.csv("Tasas Brutas-inv-Alberto-2002-2016.csv")
tinv_cris<-read.csv("Tasas Brutas-inv-Cristian-2002-2016.csv")
tsanos_alb<-read.csv("Tasas Brutas-sanos-Alberto-2010-2016.csv")
tsanos_albinc<-read.csv("Tasas Brutas-sanos-Alberto-2010-2016-sin inconsis.csv")
tsanos_cris<-read.csv("Tasas Brutas-sanos-Cristian-2010-2016.csv")
tsanos_crisinc<-read.csv("Tasas Brutas-sanos-Cristian-2010-2016-sin inconsis.csv")
tsanos_sbs = read.csv("Tabla_mortalidad-totales.csv")
##Grafica
pdf(file = "C:/Users/PC01/Downloads/Tasas Brutas/grafica_sanos.pdf",width=11,height=8,paper='special') 
plot(tsanos_alb$Edad, tsanos_alb$qx, type="l", col="blue", lwd=2,
	main="Comparación de Tasas Bruta de mortalidad para sanos",
	xlab="Edades", ylab="AAx",xlim=c(0,110), ylim=c(0,max(tsanos_sbs$qx)), xaxt="n")
lines(tsanos_albinc$qx,type="l", col="orange", lwd=2)
lines(tsanos_cris$qx,type="l", col="green", lwd=2)
lines(tsanos_crisinc$qx,type="l", col="red", lwd=2)
lines(tsanos_sbs$qx,type="l", col="black", lwd=2)
legend("topleft",col=c("blue","orange","green","red", "black"),
	legend =c("qx_enf_A", "qx_enf_A_Inc", "qx_enf_B", "qx_enf_B_Inc", "qx_SBS"), lwd=2, bty = "n")
axis(1, at = c(0:110,3), cex.axis=0.8)
dev.off()


RMSE = function(m, o){
  sqrt(mean((m - o)^2))
}

RMSE(tsanos_alb$qx,tsanos_sbs$qx_T)
RMSE(tsanos_albinc$qx,tsanos_sbs$qx_T) #### Tabla que se mejore
RMSE(tsanos_cris$qx,tsanos_sbs$qx_T)
RMSE(tsanos_crisinc$qx,tsanos_sbs$qx_T)

plot(log(tsanos_sbs$qx_T/tsanos_albinc$qx), type="l")
































data = readRDS("D:/data_sanos.rds")
for (year in 2010:2011){
	# Primero, nos quedamos con aquellas personas que fallecieron durante el
	# año de analisis
	data_m = data[data$tm_fec_fall>data$tm_fec_vig & !is.na(data$tm_fec_fall) & as.numeric(format(data$tm_fec_fall,"%Y"))==year & data$tm_salud == 0 &
			ifelse(is.na(data$finexposic),TRUE,data$tm_fec_fall<data$finexposic), ]
	
	#Calculamos la edad al momento de su muerte
	data_m["edad_m"] = as.data.frame(trunc((data_m$tm_fec_fall - data_m$tm_fec_nac)/365.25))
	
	#Consolidamos juntando las personas fallecidas por edad para un año determinado
	ans=table(edad = data_m["edad_m"])

	# Se guardan los resultados para revisarlos detalladamente
	write.csv(ans, paste('D:/N_muertos_', year, '.csv', sep=''), row.names=F)
	print(paste("Ya se proceso el año",year))
}