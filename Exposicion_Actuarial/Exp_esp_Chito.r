###################################################################################
# Si ya contamos con la base de datos descargada###################################
###################################################################################
#data = readRDS("D:/total.rds")
#data_sanos = readRDS("D:/data_sanos.rds")
#data_invalidos = readRDS("D:/data_invalidos.rds")

#summary(data)

data = readRDS("D:/total.rds")
#data_2002 = data[format(data$tm_fec_fall,"%Y")==2002 & !is.na(data$tm_fec_fall),]


# Bucle para generar las variables F1, F2, F3 y F4 para la exposicion
for (year in 2002:2017){
	ini = as.Date(paste("01/01/",year,sep=""), format = "%d/%m/%Y")
	fin = as.Date(paste("31/12/",year,sep=""), format = "%d/%m/%Y")
	data_esp = data[format(data$tm_fec_fall,"%Y")==year,]
	cumple = as.Date(paste(format(data_esp$tm_fec_nac,"%d/%m"),year,sep="/"), format="%d/%m/%Y")
	data_esp["esp"] = ifelse(!is.na(data_esp$tm_fec_fall) & data_esp$tm_fec_fall<cumple & data_esp$tm_fec_fall>ini,
	(data_esp$tm_fec_fall - ini)/365.25, 0)
	data_esp$edad_m = trunc((data_esp$tm_fec_fall-data_esp$tm_fec_nac)/365.25)
	ans = tapply(as.numeric(data_esp$esp),as.numeric(data_esp$edad_m),sum)


write.csv(ans, paste('D:/Chito_esp_', year, '.csv', sep=''))
print(paste("Ya se proceso el año",year))
        }
